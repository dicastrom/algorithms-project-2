#include "main.hpp"
#include "gtest/gtest.h"




//My algo tests


TEST(MyAlgoTest, Test1){
/*
This test uses my algorithm and takes in an input.txt file
It then reads through it and saves the original file to an parsedCsv vector of vectors
this vector of vectors is then passed in to my algorithm where it provides a
TextOut.txt file, which is then read in and saved into parsedCsv2
It then expect equality between the [0][0] th elements of the two vectors of vectors


// */
  int dimension_x = 256;
  string line;

  vector<vector<int>> parsedCsv;
  ifstream data("input.txt");
    while(getline(data,line))
    {
      stringstream lineStream(line);
      string cell;
      vector<int> parsedRow;
      while(getline(lineStream,cell,','))
      {
          int push= stoi(cell);
          parsedRow.push_back(push);
      }
      parsedCsv.push_back(parsedRow);
    }
    data.close();

    myAlgo(parsedCsv, dimension_x, "TestOut.txt");
    string line2;
    vector<vector<int>> parsedCsv2;
    ifstream data2("TestOut.txt");
      while(getline(data2,line2))
      {
        stringstream lineStream(line2);
        string cell;
        vector<int> parsedRow2;
        while(getline(lineStream,cell,','))
        {
            int push= stoi(cell);
            parsedRow2.push_back(push);
        }
        parsedCsv2.push_back(parsedRow2);
      }
      data2.close();
      //Please note we are NOT using the algorithm here, we simply want to read in the result
  EXPECT_EQ(parsedCsv[0][0],parsedCsv2[0][0]);
}



TEST(MyAlgoTest, Test2){
/*
This test reads in a file that is all zeroes except for a couple lines on the top which are
255, in picture format, this blank.txt file is all black expet for two lines up top, which are
white. This test makes sure that the value of the bottom row is still 0, after my algorithm
runs.
// */
  int dimension_x = 256;
  string line;

  vector<vector<int>> parsedCsv;
  ifstream data("blank.txt");
    while(getline(data,line))
    {
      stringstream lineStream(line);
      string cell;
      vector<int> parsedRow;
      while(getline(lineStream,cell,','))
      {
          int push= stoi(cell);
          parsedRow.push_back(push);
      }
      parsedCsv.push_back(parsedRow);
    }
    data.close();

    myAlgo(parsedCsv, dimension_x, "TestOut.txt");
    string line2;
    vector<vector<int>> parsedCsv2;
    ifstream data2("TestOut.txt");
      while(getline(data2,line2))
      {
        stringstream lineStream(line2);
        string cell;
        vector<int> parsedRow2;
        while(getline(lineStream,cell,','))
        {
            int push= stoi(cell);
            parsedRow2.push_back(push);
        }
        parsedCsv2.push_back(parsedRow2);
      }
      data2.close();

  EXPECT_EQ(0,parsedCsv2[500][500]);
}

TEST(basicFuncTest, avg)
{
  int x = 50;
  int y = 100;
  int res= avg(x,y);

  EXPECT_EQ(75,res);


}


//NN Tests



TEST(NNTest, Test1){
/*
This test uses my Nearest Neaigbour algorithm and takes in an input.txt file
It then reads through it and saves the original file to an parsedCsv vector of vectors
this vector of vectors is then passed in to my algorithm where it provides a
TextOut.txt file, which is then read in and saved into parsedCsv2
It then expect equality between the [0][0] th elements of the two vectors of vectors


// */
  int dimension_x = 256;
  string line;

  vector<vector<int>> parsedCsv;
  ifstream data("input.txt");
    while(getline(data,line))
    {
      stringstream lineStream(line);
      string cell;
      vector<int> parsedRow;
      while(getline(lineStream,cell,','))
      {
          int push= stoi(cell);
          parsedRow.push_back(push);
      }
      parsedCsv.push_back(parsedRow);
    }
    data.close();

    NearestNeighbour(parsedCsv, dimension_x, "TestOut.txt");
    string line2;
    vector<vector<int>> parsedCsv2;
    ifstream data2("TestOut.txt");
      while(getline(data2,line2))
      {
        stringstream lineStream(line2);
        string cell;
        vector<int> parsedRow2;
        while(getline(lineStream,cell,','))
        {
            int push= stoi(cell);
            parsedRow2.push_back(push);
        }
        parsedCsv2.push_back(parsedRow2);
      }
      data2.close();
      //Please note we are NOT using the algorithm here, we simply want to read in the result
  EXPECT_EQ(parsedCsv[0][0],parsedCsv2[0][0]);
}

TEST(NNTest, Test2){
/*
Same as the Previous test but for Nearest Neighbour Instead

// */
  int dimension_x = 256;
  string line;

  vector<vector<int>> parsedCsv;
  ifstream data("blank.txt");
    while(getline(data,line))
    {
      stringstream lineStream(line);
      string cell;
      vector<int> parsedRow;
      while(getline(lineStream,cell,','))
      {
          int push= stoi(cell);
          parsedRow.push_back(push);
      }
      parsedCsv.push_back(parsedRow);
    }
    data.close();

    NearestNeighbour(parsedCsv, dimension_x, "TestOut.txt");
    string line2;
    vector<vector<int>> parsedCsv2;
    ifstream data2("TestOut.txt");
      while(getline(data2,line2))
      {
        stringstream lineStream(line2);
        string cell;
        vector<int> parsedRow2;
        while(getline(lineStream,cell,','))
        {
            int push= stoi(cell);
            parsedRow2.push_back(push);
        }
        parsedCsv2.push_back(parsedRow2);
      }
      data2.close();

  EXPECT_EQ(0,parsedCsv2[500][500]);
}








int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();

}
