#include<vector>
#include<algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include<string>
#include <cmath>
#include "main.hpp"
using namespace std;

int main(int argc, char **argv)
{

  if (argc == 3 )
  {       string file_out = argv[2];
          string file_in = argv[1];
          int dimension_x = 256;
          string line;
          vector<vector<int>> parsedCsv;
          ifstream data(file_in);
        while(getline(data,line))
        {
            stringstream lineStream(line);
            string cell;
            vector<int> parsedRow;
            while(getline(lineStream,cell,','))
            {
                int push= stoi(cell);
                parsedRow.push_back(push);
            }
            parsedCsv.push_back(parsedRow);
        }
        data.close();
        myAlgo(parsedCsv, dimension_x, file_out);
        //file_out="Nearest-Neighbour-"+file_out;
        //NearestNeighbour(parsedCsv,dimension_x,file_out);
  }
  else{
    cout<<"Error: wrong number of arguments\nusage: main file_in.txt file_out.txt\n";

  }
  return 0;
}
