//#pragma once
#include<vector>
#include<algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include<string>
#include <cmath>

using namespace std;

int avg(int x, int y);
void myAlgo(vector<vector<int>> parsedCsv,int dimension_x, string file_out);
/*This is my algorithm, it mostly uses bilinear Interpolation and a tiny tiny
bit of nearest neighbor on the bottom edges */
void NearestNeighbour(vector<vector<int>> parsedCsv,int dimension_x, string file_out);


int avg(int x, int y)
{

  return ((x+y)/2);
}



//This does some bi-linear Interpolation, and nearest neighbor for the bottom/right edges

void myAlgo(vector<vector<int>> parsedCsv,int dimension_x, string file_out)
{
  int result[512][512]={};
  //this loops maps the values from the vector that was passed in into
  //our result array
  for (int i=0; i<dimension_x;i++)
      {
        for(int j=0; j<dimension_x;j++)
          {
            //cout<<parsedCsv[i][j]<<" ";
            result[i*2][j*2]=parsedCsv[i][j];

          }
          //cout<<endl;
      }
      //this loop "fills in " the missing pixels by taking the average of
      //the rows that already have values in them
      //on the bottom edge it just uses nearest neighbor alogrithm
      //it only changes the ones that already had a defaul value of 0 inside them

      for (int i=0; i<dimension_x*2;i++)
          {
            for(int j=0; j<dimension_x*2;j++)
              {
                if(j== (dimension_x*2)-1){
                  result[i][j]=result[i][j-1];
                }
                  else if(result[i][j]== 0 && j!=(dimension_x*2)-1 && i%2==0){
                  result[i][j]=avg(result[i][j-1],result[i][j+1]);
                }
              }

          }
          //this loop fills in the other values that were missing by taking the
          //average of their neighbours
          for (int i=0; i<dimension_x*2;i++)
              {
                for(int j=0; j<dimension_x*2;j++)
                  {
                    if(i%2 == 1 && i!=(dimension_x*2)-1){
                      result[i][j]=avg(result[i-1][j],result[i+1][j]);
                    }
                    if(i==(dimension_x*2)-1){
                      result[i][j]=result[i-1][j];
                    }
                  }
              }
              ofstream myfile;
              myfile.open(file_out);
              for (int i=0; i<dimension_x*2;i++)
                  {
                    for(int j=0; j<dimension_x*2;j++)
                      {
                        if(j!=(dimension_x*2) -1)
                        {
                          myfile<<result[i][j]<<",";
                        }
                        else{
                          myfile<<result[i][j];
                        }

                      }
                      myfile<<endl;
                  }
             myfile.close();
}

void NearestNeighbour(vector<vector<int>> parsedCsv,int dimension_x, string file_out)
{
  int result[512][512]={};

  for (int i=0; i<dimension_x;i++)
      {
        for(int j=0; j<dimension_x;j++)
          {
            result[i*2][j*2]=parsedCsv[i][j];
            result[(i*2)+1][j*2]= parsedCsv[i][j];
            result[(i*2)][(j*2)+1]= parsedCsv[i][j];
            result[(i*2)+1][(j*2)+1]= parsedCsv[i][j];
          }
      }
//Printing out to the file
  ofstream myfile;
  myfile.open(file_out);
  for (int i=0; i<dimension_x*2;i++)
      {
        for(int j=0; j<dimension_x*2;j++)
          {
            if(j!=(dimension_x*2) -1)
            {
              myfile<<result[i][j]<<",";
            }
            else{
              myfile<<result[i][j];
            }

          }
          myfile<<endl;
      }
      myfile.close();
}
