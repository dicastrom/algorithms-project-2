import matplotlib.pyplot as plt
from numpy import genfromtxt

input_file = input("Enter the filename: ")
my_data = genfromtxt(input_file, delimiter=',')
#this makes an array from a csv file
plt.imshow(my_data, cmap="gray")
#this makes a image with the gray color-map which only takes in one parameter per pixel
plt.show()







