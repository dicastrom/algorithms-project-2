# Algorithms Programming Assignment - II Image Upsampling

This is an image upsampler, it uses bi-linear interpolation to expand the inputed 256x256 pixel image to a 512 x 512 resolution image. In addition to, bi-linear interpolation, it also uses a bit of nearest neighbour on the bottom and right edges. 

Time spent: **12** hours spent in total

## main.cpp
The following functionality is completed:

- [X] Two different upscaling algorithms are implemented, Nearest Neighbour and Bi-Linear interpolation
- [X] Reads in a file of comma separated values with 256 rows and 256 columns (argv[1]) and outputs a csv file of 512 rows and 512 columns with the name of argv[2] 
- [X] Has a time complexity of O(n), where N is the number of pixels contained in the file
- [X] Saves two files when the program is run one is 
## Usage: 

-to compile:  g++ -std=c++11 main.cpp -lm -o main
- to use: main input_file.txt output_file.txt
Please Note: 
Since nearest neighbour was previously not being used, I made it so that it saves the output file as output_file.txt and NearestNeighbour_output_file.txt. By doing this both image upsampling algorithm are run. If you do not want the Nearest Neighbour algorithm, please comment out the following line: "  NearestNeighbour(parsedCsv,dimension_x,file_out);". 
## Alternatively
- Download and Have the Makefile, and type "make". Please Note: You Must have Gcovr and Gtest installed on your machine prior to this, see bottom for instructions on how to download Gtest/Gcovr.

<hr>

The following additional features are implemented:

- [X] csvToGrey.py, a small python program that reads in a csv file and outputs a greyscale image of it
- [X] LDist.cpp, a c++ program that finds the L1 distance between two 512x512 greyscale images in csv format. 


## csvToGrey.py

Usage: have the input file in the same directory and the csvToGrey.py program. Run the program and when prompted type in the file name, the greyscale image will appear shortly.

## LDist.cpp

-to compile: g++ -std=c++11 Ldist.cpp -lm -o L1Dist
-to use: L1Dist file1.txt file2.txt
- It will then print the L1Dist between the files



# Example of the algorithm: 

## Input Image 1:
![Inline image](input1.png)
A 256 x 256 image.

## Output Image 1 (Using Bi-linear):
![Inline image](output1.png)
A 512 x 512 image
## Output Image 1 (Using Nearest Neighbour):
![Inline image](NNOutput.png)
A 512 x 512 image
## Original Image:
![Inline image](original1.png)
A 512 x 512 image
<br>
As it can be seen the original image clearly looks the best which means that some information is lost when the algorithm is used to decompress the original image. However, my implemenation does look a lot better than both the input image and the nearest neighbour implemenation. 

## Input Image 2:
![Inline image](input2.png)
A 256 x 256 image.

## Output Image 2 (Using Bi-linear):
![Inline image](output2.png)
A 512 x 512 image
## Output Image 2 (Using Nearest Neighbour):
![Inline image](NNOutput2.png)
A 512 x 512 image
## Original Image:
![Inline image](original2.png)
A 512 x 512 image

# Testing:
Unit testing was a big part of this project. We had to learn how to install both gtest and gcover, as well as learn how to make tests. 
In order to test, you must use the makefile (and have gcover/gtest installed). To test, first type in "make all", one that runs, you should see the output of the coverage. To see the tests, type in "./test" and run. This shoudl show the tests and the result of the tests.
<hr>
## Test 1:
My test 1 was testing how my algorithm runs and works. My algorithm would read in a text file, expand it and see if the top left corner of the original image and the new expaned image are the same. This test therefore tests the algorithm itself as well as the expected output. This test is ran for both Nearest neighbour algorithm and my own bi-linear algorithm.
<hr>
##Test 2:
My test two reads in a different "blank.txt" file which is all 0's expect for the top which is all 255's. In more simple terms this is a black image with a few white horizontal line up top. The test then ensures that the bottom of the new, expended image still only contains all 0's , ensuring that the algorithm works properly. This test is ran for both Nearest neighbour algorithm and my own bi-linear algorithm.
## Function Tests:
Other tests are also there to ensure that other simpler functions are working properly.

<hr>
# Coverage:
To see the coverage, simply type in make coverage, if it says no changes simply type in "make clean; make coverage" this should output just the coverage.
## Making the coverage prettier:
Assuming gcover is installed, in order to better see how the coverage is working the following commands can be used:
~/.local/bin/gcovr -r . --html > coverage.html
~/.local/bin/gcovr -r . --html --html-details -o coverage-complete.html
These will produce two html files which show the coverage in more detail, even going live by line showing what is and what is not covered.


# Overall: 
So, yes I will be the first to admit that this algorithm is far from perfect. It looses a lot of the crispness of the original image and it does look somewhat blurry. I do think that it helps smoothing out the rough edges of the original picture and in all honestly I do think it looks better that the Nearest Neighbour algorithm, even though the L score of the Nearest Neighbour is sometimes better. 

## Installing GoogleTest:
1. cd ~
2. git clone https://github.com/google/googletest.git
3. cd googletest
4. mkdir build
5. cd build
6. cmake ..
7. pico ../ CMakeLists.txt
8. Add SET(CMAKE_CXX_FLAGS "-std=c++0x") in the begining of the file and save it
9. Now being in build folder itself give “make” command

## Installing gcovr:
1. cd ~
2. python3 -m pip install git+ https://github.com/gcovr/gcovr.git --user





