all: main test coverage
main: main.cpp
	g++ -std=c++11 -lm -o main main.cpp
test: UnitTest.cpp
	g++ -std=c++11 -I ~/googletest/googletest/include -L ~/googletest/build/lib UnitTest.cpp -o test -lgtest -lpthread
	./test
coverage:
	g++ -std=c++11 -I ~/googletest/googletest/include -L ~/googletest/build/lib -fprofile-arcs -ftest-coverage -fPIC UnitTest.cpp -o coverage -lgtest -lpthread
	./coverage
	./coverage input.txt output.txt
	~/.local/bin/gcovr -r .


clean:
	rm main test coverage
